import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingBasketTests {

    private ShoppingBasket basket;

    @BeforeEach
    public void initBasket() {
        basket = new ShoppingBasket();
    }

    @Test
    public void checkOperationForBasket() throws NoSuchProductException {

        assertEquals(0, basket.countProducts());
        assertEquals(0, basket.getBasketPrice());

        Product p1 = new Product(100, "Fiat", 3, 1);

        assertFalse(basket.removeProduct(1));
        assertTrue(basket.addProduct(p1));

        assertEquals(3, basket.countProducts());
        assertEquals(100, basket.getBasketPrice());

        assertTrue(basket.removeProduct(1));
        assertEquals(0, basket.countProducts());
        assertEquals(0, basket.getBasketPrice());

        assertTrue(basket.addProduct(new Product(100, "Fiat" , 1, 1)));
        assertTrue(basket.addProduct(new Product(200, "Opel" , 2, 2)));
        assertTrue(basket.addProduct(new Product(300, "Ferrari" , 3, 3)));
        assertTrue(basket.addProduct(new Product(400, "Subaru" , 4, 4)));

        assertNotEquals(0, basket.countProducts());
        assertNotEquals(0, basket.getBasketPrice());

        assertEquals(10, basket.countProducts());
        assertEquals(1000, basket.getBasketPrice());

        assertEquals(100, basket.getProductPrice(1));
        assertEquals(200, basket.getProductPrice(2));
        assertEquals(300, basket.getProductPrice(3));
        assertEquals(400, basket.getProductPrice(4));

        assertTrue(basket.removeProduct(2));
        assertEquals(300, basket.getProductPrice(3));
    }
}
