public interface Basket {

    public boolean addProduct(Product prod);

    boolean removeProduct(int id);

    int countProducts();

    int getProductPrice(int id) throws NoSuchProductException;

    int getBasketPrice();
}
