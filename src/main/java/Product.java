public class Product {

    private int price;
    private String name;
    private int quantity;
    private int id;

    public Product(int price, String name, int quantity, int id) {
        this.price = price;
        this.name = name;
        this.quantity = quantity;
        this.id = id;
    }

    public int getPrice() {
        return this.price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }
}
