import java.util.ArrayList;

public class ShoppingBasket implements Basket {

    private ArrayList<Product> products = new ArrayList<Product>();

    @Override
    public boolean addProduct(Product prod) {
        this.products.add(prod);
        return true;
    }

    @Override
    public boolean removeProduct(int id) {
        for(Product prod: products) {
            if(prod.getId() == id) {
                products.remove(prod);
                return true;
            }
        }
        return false;
    }

    @Override
    public int countProducts() {
        int count = 0;
        for(Product prod: products) {
            count += prod.getQuantity();
        }
        return count;
    }

    @Override
    public int getProductPrice(int id) throws NoSuchProductException {
        for(Product prod: products) {
            if(prod.getId() == id) {
                return prod.getPrice();
            }
        }
        throw new NoSuchProductException();
    }

    @Override
    public int getBasketPrice() {
        int sum = 0;
        for(Product prod: products) {
            sum += prod.getPrice();
        }
        return sum;
    }
}
